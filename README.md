# Project Test Tommy



## Getting started

- [ ] Pembuatan base template menggunakan laravel
- [ ] Pembuatan migration class untuk tabel MS_PRODUCT berisi ID, NamaProduk, Qty
- [ ] Pembuatan modul login (migration class dan halaman). Credential menggunakan username dan password
- [ ] Pembuatan halaman dashboard berisi statistik jumlah user dan jumlah produk terdaftar
- [ ] Pembuatan RESTFul API yaitu endpoint untuk inquiry produk, menggunakan credential Oauth2.0/JWT/simple auth
